import { Platform } from 'react-native';
import colors from './colors';
import metrics from './metrics';
import fonts from './fonts';

export default {
    structure: {
        flex: 1,
    },
    container: {
        flex: 1,
        padding: metrics.basePadding,
    },
    backgroundImage: {
        width: '100%',
        heigth: '100%',
    },
    row: {
        width: '100%',
        heigth: 40,
    },
    label: {
      fontSize: 16,
      color: colors.pink,
      fontFamily: fonts.r700,
    },
    textInput: {
      height: 49,
      paddingVertical: Platform.OS==='android'? 0: 10,
      marginBottom: 10,
      width: '100%',
      backgroundColor: 'transparent',
      borderBottomColor: colors.borderInput,
      borderBottomWidth: 1,
      fontSize: 16,
      color: colors.borderInput,
      fontFamily: fonts.r400,
    },
    textInputArea: {
      paddingTop: Platform.OS==='android'?20:20,
      paddingBottom: Platform.OS==='android'? 150: 150,
      paddingHorizontal: 20,
      marginBottom: 10,
      marginTop: 10,
      width: '100%',
      borderRadius: 10,
      backgroundColor: 'transparent',
      borderColor: colors.borderInput,
      borderWidth: 1,
      fontSize: 16,
      color: colors.borderInput,
      fontFamily: fonts.r400,
    },
    pickerInput: {
      height: Platform.OS==='android'? 49: 10,
      paddingVertical: Platform.OS==='android'? 0: 5,
      marginBottom: 10,
      width: '100%',
      backgroundColor: 'transparent',
      borderBottomColor: colors.borderInput,
      borderBottomWidth: 1,
      fontSize: 16,
      color: colors.borderInput,
      fontFamily: fonts.r400,
    },
    subText1: {
      fontFamily: fonts.r400,
      color: colors.subTxt1,
      fontSize: 16
    },
    subText2: {
      fontFamily: fonts.r400,
      color: colors.subTxt2,
      fontSize: 20
    },
    subText3: {
      fontFamily: fonts.r700,
      color: colors.facebook,
      fontSize: 18,
      padding: metrics.basePadding,
    },
    p: {
      fontFamily: fonts.r400,
      color: colors.subTxt1,
      fontSize: 16,
    },
    flexColumn: {
      flexDirection: 'column'
    },
    flexRow: {
      flexDirection: 'row'
    },

};
