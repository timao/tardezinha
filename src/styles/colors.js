export default {
    white: '#FFFFFF',
    purple: '#706CB0',
    gray: '#D8D8D8',
    borderInput: '#C1C1C1',
    pink: '#EA5297',
    subTxt1: '#A2A0A0',
    subTxt2: '#5B5B5B',
    facebook: '#5BC5F2',
    google: '#E73458',
    backgroundBanner: '#F6F6F6',
    greenmoss: '#57563D',
    greenmosslight: '#9C936B',
    orange: '#F49F42',
    divisor: '#E3E3E3'
  };
