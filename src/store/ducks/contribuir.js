import Immutable from 'seamless-immutable';

export const Types = {
  GET_REQUEST: 'contribuir/GET_REQUEST',
  GET_SUCCESS: 'contribuir/GET_SUCCESS',
  GET_FAILURE: 'contribuir/GET_FAILURE',
};

const initialState = Immutable({
  data: [],
  loading: false,
  error:false,
  error_message:''
});

export default function contribuir(state = initialState, action) {
  switch (action.type) {
    case Types.GET_REQUEST:
      return { ...state, loading: true };
    case Types.GET_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        loading: false,
        error: false,
      };
    case Types.GET_FAILURE:
    return { ...state, loading: false, error: true, alert: true, error_message: action.payload.response.data.Mensagem};
    default:
      return state;
  }
}


export const Creators = {

  getContribuirRequest: Usuario => ({
    type: Types.GET_REQUEST,
    payload: {
      Usuario,
    },
  }),

  getContribuirSuccess: data => ({
    type: Types.GET_SUCCESS,
    payload: {
      data,
    },
  }),

  getContribuirFailure: error => ({
    type: Types.GET_FAILURE,
    payload: error,
  }),



};
