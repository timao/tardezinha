import Immutable from 'seamless-immutable';

export const Types = {
  GET_REQUEST: 'assinatura/GET_REQUEST',
  GET_SUCCESS: 'assinatura/GET_SUCCESS',
  GET_FAILURE: 'assinatura/GET_FAILURE',
};

const initialState = Immutable({
  data: '',
  loading: false,
  alert: false,
  message: '',
  icon: '',
  error:false,
  error_message:''
});


export default function assinatura(state = initialState, action) {
  switch (action.type) {
    case Types.GET_REQUEST:
      return { ...state, loading: true };
    case Types.GET_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        loading: false,
        error: false,
      };
    case Types.GET_FAILURE:
      return {
        ...state,
        error: true,
        loading: false,
        error_message: action.payload.response.data.Message,
       };

    default:
      return state;
  }
}


export const Creators = {

  getAssinaturaRequest: Usuario => ({
    type: Types.GET_REQUEST,
    payload: {
      Usuario,
    },
  }),


  getAssinaturaSuccess: data => ({
    type: Types.GET_SUCCESS,
    payload: {
      data,
    },
  }),

  getAssinaturaFailure: error => ({
    type: Types.GET_FAILURE,
    payload: error,
  }),



};
