import Immutable from 'seamless-immutable';

export const Types = {
  GET_REQUEST: 'faleconosco/GET_REQUEST',
  GET_SUCCESS: 'faleconosco/GET_SUCCESS',
  GET_FAILURE: 'faleconosco/GET_FAILURE',
};

const initialState = Immutable({
  data: [],
  loading: false,
  error:false,
  error_message:''
});

export default function faleconosco(state = initialState, action) {
  switch (action.type) {
    case Types.GET_REQUEST:
      return { ...state, loading: true };
    case Types.GET_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        loading: false,
        error: false,
      };
    case Types.GET_FAILURE:
      return { ...state, loading: false, error:true, error_message: action.payload.response.data.Message,};
    default:
      return state;
  }
}


export const Creators = {

  getFaleConoscoRequest: Usuario => ({
    type: Types.GET_REQUEST,
    payload: {
      Usuario,
    },
  }),

  getFaleConoscoSuccess: data => ({
    type: Types.GET_SUCCESS,
    payload: {
      data,
    },
  }),

  getFaleConoscoFailure: error => ({
    type: Types.GET_FAILURE,
    payload: error,
  }),



};
