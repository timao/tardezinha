import Immutable from 'seamless-immutable';

export const Types = {
  GET_REQUEST: 'timeline/GET_REQUEST',
  GET_SUCCESS: 'timeline/GET_SUCCESS',
  GET_FAILURE: 'timeline/GET_FAILURE',
};

const initialState = Immutable({
  data: [],
  loading: false,
  error:false,
  error_message:''
});

export default function timeline(state = initialState, action) {
  switch (action.type) {
    case Types.GET_REQUEST:
      return { ...state, loading: true };
    case Types.GET_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        loading: false,
        error: false,
      };
    case Types.GET_FAILURE:
      return { ...state, loading: false, error:true, error_message: action.payload.error_message,};
    default:
      return state;
  }
}


export const Creators = {

  getTimelineRequest: Usuario => ({
    type: Types.GET_REQUEST,
    payload: {
      Usuario,
    },
  }),

  getTimelineSuccess: data => ({
    type: Types.GET_SUCCESS,
    payload: {
      data,
    },
  }),

  getTimelineFailure: error => ({
    type: Types.GET_FAILURE,
    payload: error,
  }),



};
