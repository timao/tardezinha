import Immutable from 'seamless-immutable';

export const Types = {
  GET_REQUEST: 'facebook/GET_REQUEST',
  GET_SUCCESS: 'facebook/GET_SUCCESS',
  GET_FAILURE: 'facebook/GET_FAILURE',
};

const initialState = Immutable({
  data: [],
  loading: false,
  error:false,
  error_message:''
});

export default function facebook(state = initialState, action) {
  switch (action.type) {
    case Types.GET_REQUEST:
      return { ...state, loading: true };
    case Types.GET_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        loading: false,
        error: false,
      };
    case Types.GET_FAILURE:
      return { ...state, loading: false, error:true, error_message: action.payload.error_message,};
    default:
      return state;
  }
}


export const Creators = {

  getFacebookRequest: Usuario => ({
    type: Types.GET_REQUEST,
    payload: {
      Usuario,
    },
  }),

  getFacebookSuccess: data => ({
    type: Types.GET_SUCCESS,
    payload: {
      data,
    },
  }),

  getFacebookFailure: error => ({
    type: Types.GET_FAILURE,
    payload: error,
  }),



};
