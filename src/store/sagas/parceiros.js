import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import { Creators as ParceirosActions } from '../ducks/parceiros';

export function* getParceiros(action) {
  const { Usuario } = action.payload;

  const token = Usuario.replace('"', '').replace('"', '')

  const headers = {
    headers: {
      'appid': 'diretoria-do-bem',
      'Authorization': 'bearer ' + token,
      'Content-Type': 'application/json',
    },
  };
  try{
    const response = yield call(api.get, `/Conteudos/8`, headers);
    yield put(ParceirosActions.getParceirosSuccess(response.data));
  }catch (err){
    yield put(ParceirosActions.getParceirosFailure(err));
  }
}
