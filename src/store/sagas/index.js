import { all, spawn, takeEvery, takeLatest } from "redux-saga/effects";

import { Types as UserTypes } from '../ducks/user';
import { Types as VersaoTypes } from '../ducks/versao';
import { Types as LoginTypes } from '../ducks/login';
import { Types as CadastroTypes } from '../ducks/cadastro';
import { Types as UploadTypes } from '../ducks/upload';
import { Types as EventosTypes } from '../ducks/eventos';
import { Types as TimelineTypes } from '../ducks/timeline';
import { Types as AgendaTypes } from '../ducks/agenda';
import { Types as FazeroBemTypes } from '../ducks/fazerobem';
import { Types as LojinhaTypes } from '../ducks/lojinha';
import { Types as AturneTypes } from '../ducks/aturne';
import { Types as SobreTypes } from '../ducks/sobre';
import { Types as ArrecadacaoTypes } from '../ducks/arrecadacao';
import { Types as ParceirosTypes } from '../ducks/parceiros';
import { Types as VoucherTypes } from '../ducks/voucher';
import { Types as FaqTypes } from '../ducks/faq';
import { Types as FaleConoscoTypes } from '../ducks/faleconosco';
import { Types as EsqueciSenhaTypes } from '../ducks/esquecisenha';
import { Types as EditarPerfilTypes } from '../ducks/editarperfil';
import { Types as FacebookTypes } from '../ducks/facebook';
import { Types as ContribuirTypes } from '../ducks/contribuir';
import { Types as AssinaturaTypes } from '../ducks/assinatura';

import { startWatchingNetworkConnectivity } from "./offline";
import { getVersao } from "./versao";
import { getUser } from "./user";
import { getLogin } from "./login";
import { getCadastro } from "./cadastro";
import { getEventos } from "./eventos";
import { getUpload } from "./upload";
import { getTimeline } from "./timeline";
import { getAgenda } from "./agenda";
import { getFazeroBem } from "./fazerobem";
import { getLojinha } from "./lojinha";
import { getAturne } from "./aturne";
import { getSobre } from "./sobre";
import { getArrecadacao } from "./arrecadacao";
import { getParceiros } from "./parceiros";
import { getVoucher } from "./voucher";
import { getFaq } from "./faq";
import { getFaleConosco } from "./faleconosco";
import { getEsqueciSenha } from "./esquecisenha";
import { getEditarPerfil } from "./editarperfil";
import { getFacebook } from "./facebook";
import { getContribuir } from "./contribuir";
import { getAssinatura } from "./assinatura";

export default function* rootSaga() {
  yield all([
    spawn(startWatchingNetworkConnectivity),
    takeEvery(VersaoTypes.GET_REQUEST, getVersao),
    takeLatest(LoginTypes.GET_REQUEST, getLogin),
    takeLatest(CadastroTypes.GET_REQUEST, getCadastro),
    takeEvery(UserTypes.GET_REQUEST, getUser),
    takeLatest(UploadTypes.GET_REQUEST, getUpload),
    takeLatest(EventosTypes.GET_REQUEST, getEventos),
    takeLatest(TimelineTypes.GET_REQUEST, getTimeline),
    takeLatest(AgendaTypes.GET_REQUEST, getAgenda),
    takeLatest(FazeroBemTypes.GET_REQUEST, getFazeroBem),
    takeLatest(LojinhaTypes.GET_REQUEST, getLojinha),
    takeLatest(AturneTypes.GET_REQUEST, getAturne),
    takeLatest(SobreTypes.GET_REQUEST, getSobre),
    takeLatest(ArrecadacaoTypes.GET_REQUEST, getArrecadacao),
    takeLatest(ParceirosTypes.GET_REQUEST, getParceiros),
    takeLatest(VoucherTypes.GET_REQUEST, getVoucher),
    takeLatest(FaqTypes.GET_REQUEST, getFaq),
    takeLatest(FaleConoscoTypes.GET_REQUEST, getFaleConosco),
    takeLatest(EsqueciSenhaTypes.GET_REQUEST, getEsqueciSenha),
    takeLatest(EditarPerfilTypes.GET_REQUEST, getEditarPerfil),
    takeLatest(FacebookTypes.GET_REQUEST, getFacebook),
    takeLatest(ContribuirTypes.GET_REQUEST, getContribuir),
    takeLatest(AssinaturaTypes.GET_REQUEST, getAssinatura),
  ]);
}
