import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import queryString from 'query-string';
import config from "../../config/config";

import { Creators as FacebookActions } from '../ducks/facebook';

export function* getFacebook(action) {
  const { Usuario } = action.payload;

  const token = Usuario.token.replace('"', '').replace('"', '')

  const params = queryString.stringify({
    IdFacebook: Usuario.IdFacebook,
    IdGoogle: Usuario.IdGoogle,
  });

  const headers = {
    headers: {
      'Authorization': 'bearer ' + token,
      'Content-Type': 'application/json',
    },
  };

  try{
    const response = yield call(api.post, `/Usuarios/AssociarRedeSocial`, params, headers);
    yield put(FacebookActions.getFacebookSuccess(response.data));
  }catch (err){
    yield put(FacebookActions.getFacebookFailure(err));
  }
}
