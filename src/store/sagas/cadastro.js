import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import queryString from 'query-string';
import { Creators as CadastroActions } from '../ducks/cadastro';

export function* getCadastro(action) {
  const { Usuario } = action.payload;
  let cpfconvert = Usuario.CPF.replace(/[^\d]+/g,'')
  const cepconvert = Usuario.Cep.replace("-", "");

  const params = {
    Foto: Usuario.Foto,
    Nome: Usuario.Nome,
    Sobrenome: Usuario.Sobrenome,
    Email: Usuario.Email,
    CPF: cpfconvert,
    DataNascimento: Usuario.ano + '-' + Usuario.mes + '-' + Usuario.dia,
    Celular: Usuario.Celular,
    Senha: Usuario.Senha,
    ConfirmarSenha: Usuario.ConfirmarSenha,
    Endereco: Usuario.Endereco,
    Cidade: Usuario.Cidade,
    CEP: cepconvert,
    ConfirmarSenha: Usuario.ConfirmarSenha,
    IdFacebook: Usuario.IdFacebook,
    IdGoogle: Usuario.IdGoogle,
    GoogleSubscriptionId: Usuario.GoogleSubscriptionId,
    GooglePlayId: Usuario.GooglePlayId,
    GooglePurchaseToken: Usuario.GooglePurchaseToken,
    AppStoreSubscriptionId: Usuario.AppStoreSubscriptionId,
    AppStoreId: Usuario.AppStoreId,
    AppStorePurchaseToken: Usuario.AppStorePurchaseToken,
  };

  const headers = {
    headers: {
      appid: 'diretoria-do-bem',
      'Content-Type': 'application/json',
    },
  };

  try{
    const response = yield call(api.post, `/Usuarios/Cadastro`, params, headers);
    yield put(CadastroActions.getCadastroSuccess(response.data));
  }catch (err){
    yield put(CadastroActions.getCadastroFailure(err));
  }
}
