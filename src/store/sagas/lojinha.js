import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import { Creators as LojinhaActions } from '../ducks/lojinha';

export function* getLojinha(action) {
  const { Usuario } = action.payload;

  const token = Usuario.replace('"', '').replace('"', '')

  const headers = {
    headers: {
      'appid': 'diretoria-do-bem',
      'Authorization': 'bearer ' + token,
      'Content-Type': 'application/json',
    },
  };

  try{
    const response = yield call(api.get, `/Conteudos/9`, headers);
    yield put(LojinhaActions.getLojinhaSuccess(response.data));
  }catch (err){
    yield put(LojinhaActions.getLojinhaFailure(err));
  }
}
