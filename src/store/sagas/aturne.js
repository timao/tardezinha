import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import { Creators as AturneActions } from '../ducks/aturne';

export function* getAturne(action) {
  const { Usuario } = action.payload;

  const token = Usuario.replace('"', '').replace('"', '')

  const headers = {
    headers: {
      'appid': 'diretoria-do-bem',
      'Authorization': 'bearer ' + token,
      'Content-Type': 'application/json',
    },
  };
  try{
    const response = yield call(api.get, `/Paginas/6`, headers);
    yield put(AturneActions.getAturneSuccess(response.data));
  }catch (err){
    yield put(AturneActions.getAturneFailure(err));
  }
}
