import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import queryString from 'query-string';

import { Creators as EsqueciSenhaActions } from '../ducks/esquecisenha';

export function* getEsqueciSenha(action) {
  const { Usuario } = action.payload;

  let params = {
    Email: Usuario.email,
  };

  const headers = {
    headers: {
      appid: 'diretoria-do-bem',
      'Content-Type': 'application/json',
    },
  };

  try{
    const response = yield call(api.post, `/Usuarios/RecuperarSenha`, params, headers);
    yield put(EsqueciSenhaActions.getEsqueciSenhaSuccess(response.data));
  }catch (err){
    yield put(EsqueciSenhaActions.getEsqueciSenhaFailure(err));
  }
}
