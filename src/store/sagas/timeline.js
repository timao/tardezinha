import { call, put } from "redux-saga/effects";
import api from "../../services/api";
import { Creators as TimelineActions } from '../ducks/timeline';

export function* getTimeline(action) {
  const { Usuario } = action.payload;

  const token = Usuario.replace('"', '').replace('"', '')

  const headers = {
    headers: {
      'appid': 'diretoria-do-bem',
      'Authorization': 'bearer ' + token,
      'Content-Type': 'application/json',
    },
  };

  try{
    const response = yield call(api.get, `/Conteudos/5`, headers);
    yield put(TimelineActions.getTimelineSuccess(response.data));
  }catch (err){
    yield put(TimelineActions.getTimelineFailure(err));
  }
}
