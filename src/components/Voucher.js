import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import QRCode from 'react-native-qrcode'

import { colors, fonts, metrics } from '../styles'
const Voucher = ({
    data,
    postVoucher
}) => (
    <TouchableOpacity style={[data.Expirado ?styles.buttonOpacity  :styles.button]} onPress={() => {postVoucher(data)}}>
        <View style={styles.wrapDesc}>
          <Text style={styles.h1}>{data.Evento.Titulo}</Text>
          <Text style={styles.p}>{data.Evento.Chamada}</Text>
          <Text style={styles.p}>{data.DataValidadeVoucherExtenso}</Text>
        </View>
        <Image
          source={require('../assets/icons/iconqrcode.png')}
          style={{
            resizeMode: 'contain',
            width: 80,
            height: 80
          }}
        />
    </TouchableOpacity>
);

const styles = StyleSheet.create({
  button: {
    width: '100%',
    borderRadius: 10,
    padding: metrics.basePadding*2,
    backgroundColor: colors.pink,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 15
  },
  buttonOpacity: {
    width: '100%',
    borderRadius: 10,
    padding: metrics.basePadding*2,
    backgroundColor: colors.pink,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 15,
    opacity: 0.7

  },
  wrapDesc: {
    flexDirection: 'column',
    width: '50%',
    marginRight: 15,
  },
  h1: {
    fontFamily: fonts.r700,
    color: colors.white,
    fontSize: 18,
    marginBottom: 10,
  },
  p: {
    fontFamily: fonts.r400,
    color: colors.white,
    marginBottom: 5,
    fontSize: 15,
  },
})


export default Voucher;
