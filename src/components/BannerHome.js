import React from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import Swiper from 'react-native-swiper';
import styles from '../screens/home/Home.styles';
import { colors } from '../styles';

const BannerHome = ({
    data,
    navigation
}) => (
    <View>
      <Swiper
        style={{
          height: 340,
        }}
        key={data.length}
        dot={
          <View style={{backgroundColor: colors.borderInput, width: 8, height: 8,borderRadius: 100, marginLeft: 6, marginRight: 6, marginTop: 0, marginBottom: 3,}} />
        }
        activeDot={
          <View style={{backgroundColor: colors.pink, width: 12, height: 12, borderRadius: 100, marginLeft: 3, marginRight: 3, marginTop: -1, marginBottom: 3,}} />
        }
        showsButtons={false}
        loop={false}
        showsPagination={true}
        showsButtons={false}>
        {
          data.map((data, index) => {
             return (
          <TouchableOpacity style={styles.slide1}  onPress={() => { navigation.push('EventoPage', {
            EventoDetails: data
          }) }} key={index} >
            <Image source={{uri: data.PathImagem}} style={styles.img}/>
            <View style={
              styles.container
            }>
            <Text style={styles.h1}>{data.Titulo}</Text>
            <Text style={styles.p}>{data.DataExtenso}</Text>
            </View>
          </TouchableOpacity>
             )
          })
        }
      </Swiper>
    </View>
);

export default BannerHome;
