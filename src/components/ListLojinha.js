import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet, Image, Platform } from 'react-native';
import { fonts, colors, metrics } from '../styles';

const ListLojajinha = ({
  data,
  open
}) => (
    <View style={styles.row}>
        {
          data.map((data, index) => {
             return (
              <TouchableOpacity style={styles.item} key={index} onPress={() => open(data.URL)}>
                <View style={[styles.backgroundImage]}>
                  <Image
                    style={styles.imagem}
                    source={{uri: data.Imagem}}
                    />
                </View>
                <View style={styles.container}>
                  <Text style={styles.h3} numberOfLines={2} >{data.Titulo}</Text>
                  <Text style={styles.h4}>{data.Subtitulo}</Text>
                </View>
              </TouchableOpacity>
             )
          })
        }

    </View>
);

const styles = StyleSheet.create({
  row: {
    width: '98%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginRight: 'auto',
    marginLeft: 'auto',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  item: {
    width: '46%',
    borderRadius: 10,
    borderColor: 'rgba(238,238,238, 0.7)',
    borderWidth:1,
    overflow: 'hidden',
    marginBottom: 10,
    position: 'relative'
  },
  backgroundImage:{
    height: 150,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imagem: {
    width: '100%',
    height: 150,
    resizeMode: 'cover'
  },
  container: {
    width: '100%',
    padding: metrics.basePadding
  },
  h3: {
    fontFamily: fonts.r700,
    color: colors.facebook,
    fontSize: 15,
    width: '100%',
    flexWrap: 'wrap',
    flexDirection: 'column',
  },
  h4:{
    fontFamily: fonts.r400,
    color: colors.subTxt1,
    fontSize: 14,
  }
})

export default ListLojajinha;
