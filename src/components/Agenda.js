import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image, ActivityIndicator, Dimensions } from 'react-native';
import { fonts, colors, metrics } from '../styles';

class Agenda extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { data, navigation } = this.props;
    return (
      <TouchableOpacity style={{
        borderRadius: 10,
        borderColor: 'rgba(238,238,238, 0.7)',
        borderWidth:1,
        overflow: 'hidden',
        marginBottom: 10,
        position: 'relative'
      }} onPress={() => { navigation.push('EventoPage', {
        EventoDetails: data
      }) }} >
        {
          data.PathImagem
          ? <View><ActivityIndicator style={{ position: 'absolute',  top: 0, left: 0, right: 0, }} /><Image source={{ uri: data.PathImagem}} style={[styles.imagem,]}/></View>
          : null
        }
        <View style={styles.container}>
          <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
            <Text style={styles.h3}>{data.Titulo}</Text>
          </View>
          <Text style={styles.h4}>{data.DataExtenso}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  box_header: {

  },
  header: {
    padding: metrics.basePadding*2,
    width: '100%',
    height: 30,
    flexDirection: 'row',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    alignItems: 'center',
  },
  infoPost: {
    flexDirection: 'row'
  },
  photo: {
    width: 50,
    height: 50,
    marginLeft: 15,
    marginRight: 15,
    backgroundColor: '#000',
    borderRadius: 100,
    zIndex: 9,
    position: 'absolute',
    top: 10,
  },
  h1: {
    fontFamily: fonts.r700,
    color: colors.white,
    fontSize: 18,
    marginLeft: 60
  },
  h2: {
    fontFamily: fonts.r400,
    color: colors.white,
    fontSize: 13,
    margin: 4,
  },
  container: {
    width: '100%',
    padding: metrics.basePadding*2

  },
  h3: {
    fontFamily: fonts.r700,
    color: colors.facebook,
    fontSize: 18,
    marginBottom: 2,
  },
  h4: {
    fontFamily: fonts.r400,
    color: colors.subTxt1,
    fontSize: 14,
  },
  imagem: {
    width: '100%',
    height: 200,
  }
});

export default Agenda;