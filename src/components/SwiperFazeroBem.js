import React from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import Swiper from 'react-native-swiper';
import {BoxShadow} from 'react-native-shadow';
import HTML from 'react-native-render-html';

import Buttons from './Button'
import { metrics, colors, fonts } from '../styles';

class SwiperFazeroBem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const {
      data,
    } = this.props;
    return (
      <Swiper
      style={{
        height: 510,
      }}
      key={data.length}
      ref='swiper'
      showsButtons={true}
      loop={true}
      showsPagination={false}
      buttonWrapperStyle={{backgroundColor: 'transparent', flexDirection: 'row', position: 'absolute', top: -100, left: 0, flex: 1, paddingHorizontal: 10, paddingVertical: 10, justifyContent: 'space-between', alignItems: 'center'}}
      prevButton={
        <TouchableOpacity style={[styles.buttonArrows,{ marginLeft: 10 }]}  onPress={() => this.refs.swiper.scrollBy(-1)}>
          <Icon name='arrow-left' size={15} color={colors.white} />
        </TouchableOpacity>
      }

      nextButton={
        <TouchableOpacity style={[styles.buttonArrows,{marginRight: 10,}]} onPress={() => this.refs.swiper.scrollBy(1)}>
            <Icon name='arrow-right' size={15} color={colors.white} />
        </TouchableOpacity>
      }
      showsButtons={true}>
      {
       data.map((data, index) => {
          return (
            <View style={styles.slide1} key={index}>
              <View style={[{ backgroundColor: 'rgba(0, 0, 0, 0.6)', width: '75%', height: 300, borderRadius: 10 }]}>
                <Image source={{ uri: data.Imagem}} style={styles.img}/>
              </View>
              <View style={styles.container2}>
              <Text style={styles.h2}>{data.Titulo}</Text>
              <HTML tagsStyles={{
              }}
             html={data.Texto} tagsStyles={{
              p: styles.p2,
            }}/>
            </View>
          </View>
          )
        })
      }
    </Swiper>

    );
  }
}
const styles = StyleSheet.create({
  buttonArrows: {
    borderRadius: 10,
    width: 43,
    height: 43,
    backgroundColor: colors.pink,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 9999,
    // position: 'relative',
    // top: -100,
    // zIndex: 9999
  },
  img: {
    width: '100%',
    zIndex: 0,
    height: '100%',
    resizeMode: 'cover',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: { width: 400  , height: 500 },
    shadowOpacity: 0.8,

   // its for android
    elevation: 5,
    position:'relative',
  },
  slide1: {
    alignItems: 'center',
    width: '100%', height: '100%'
  },
  container2: {
    padding: metrics.basePadding*2,
    justifyContent: 'center',
    alignItems: 'center',
    width: '95%',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  h2: {
    fontFamily: fonts.r700,
    fontSize: 18,
    color: colors.facebook,
    marginBottom: 0,
  },
  p2: {
    fontFamily: fonts.r400,
    fontSize: 14,
    lineHeight: 21,
    color: colors.subTxt1,
    textAlign: 'center',
    width: 290
  },
})

export default SwiperFazeroBem;
