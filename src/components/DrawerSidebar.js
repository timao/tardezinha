import React, { Component } from 'react';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import { Linking, Text, View, ImageBackground, Image, SafeAreaView, TouchableOpacity, AsyncStorage, StyleSheet, Platform } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import { navigatorRef } from "../App";
import { colors, general, metrics } from '../styles'
import Icon from 'react-native-vector-icons/FontAwesome';

class DrawerSidebar extends Component {
  logout() {
    const { navigation } = this.props;

    AsyncStorage.removeItem('UserData', () => {
      const nav = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: 'LoginStack'
          })
        ],
        key: null
      });
      navigatorRef.dispatch(nav);
    })
  }

  componentDidMount = async () => {
  }

  render() {

    const { navigation } = this.props;

    return (
      <SafeAreaView style={{flex:1, backgroundColor: colors.greenmosslight}} forceInset={{ bottom: 'never' }}>
        <View style={styles.bgFundo}>
          <Text style={styles.txtClose} onPress={() => navigation.closeDrawer()}>
            <Icon name="times" color="#fff" style={styles.iconClose} />
          </Text>
         <View style={styles.listMenu}>
            <TouchableOpacity activeOpacity={0.3} style={styles.menuBtns} onPress={() => { this.props.navigation.closeDrawer(); navigation.navigate('LojasStack') }}>
              <Text style={styles.menuTxtBtns}>A TURNÊ DO TARDEZINHA</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.3} style={styles.menuBtns} onPress={() => { this.props.navigation.closeDrawer(); navigation.navigate('Inicial') }}>
              <Text style={styles.menuTxtBtns}>SOBRE O TARDEZINHA DO BEM</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.3} style={styles.menuBtns} onPress={() => { this.props.navigation.closeDrawer(); navigation.navigate('regulamento') }} >
              <Text style={styles.menuTxtBtns}>ARRECADAÇÃO</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.3} style={styles.menuBtns} onPress={() => { this.props.navigation.closeDrawer(); navigation.navigate('duvidasFrequentes') }}>
              <Text style={styles.menuTxtBtns}>PARCEIROS</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.3} style={styles.menuBtns} onPress={() => { this.props.navigation.closeDrawer(); navigation.navigate('HistoricoFaleconoscoStack') }}>
              <Text style={styles.menuTxtBtns}>MÚSICAS EXCLUSIVAS</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.3} style={styles.menuBtns} onPress={() => { this.props.navigation.closeDrawer(); navigation.navigate('HistoricoFaleconoscoStack') }}>
              <Text style={styles.menuTxtBtns}>VOUCHER</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.3} style={styles.menuBtns} onPress={() => { this.props.navigation.closeDrawer(); navigation.navigate('HistoricoFaleconoscoStack') }}>
              <Text style={styles.menuTxtBtns}>AJUDA</Text>
            </TouchableOpacity>
          </View>
      </View>
      </SafeAreaView>
  );
  }
}

const styles = StyleSheet.create({
  bgFundo: {
      backgroundColor: '#F3F3F3',
      width: '100%',
      height: metrics.screenHeight,
      position: 'relative',
  },
  headerUsuario: {
      width: '100%',
      height: 250
  },

  imgBackgroundUsuario: {
      width: '100%',
      height: '100%',
      flex: 1,
      backgroundColor: '#ffffff'
  },
  containerUsuario: {
      width: '100%',
      flexDirection: 'row',
      justifyContent: 'center',
      flexWrap: 'wrap',
      textAlign: 'center'
  },
  imgUsuario: {
      height: 110,
      width: 110,
      borderRadius: 55,
      marginTop: 25,
      borderWidth: 6,
      borderColor: colors.greenmoss,
  },
  nomeUsuario: {
      fontSize: metrics.defaultFontSizeTitle,
      color: colors.gray,
      width: '100%',
      textAlign: 'center',
      fontWeight: 'bold',
      marginTop: 8
  },
  txtPontos: {
      fontSize: metrics.defaultFontSizeTxt,
      color: colors.gray,
      textAlign: 'center',
      width: '100%'
  },
  Pontos: {
      fontSize: metrics.defaultFontSizeTxt,
      fontWeight: 'bold'
  },
  containerBtns: {
      width: '80%',
      justifyContent: 'space-between',
      marginLeft: 'auto',
      marginRight: 'auto',
      flexDirection: 'row',
      marginTop: 10
  },
  btns: {
      backgroundColor: colors.bgButton,
      width: '45%',
      height: 24,
      justifyContent: 'center',
      borderRadius: 100
  },
  txtBtns: {
      fontSize: 12,
      color: '#ffffff',
      textAlign: 'center',
      fontWeight: 'bold',
  },
  menuBtns: {
      backgroundColor: '#F3F3F3',
      height: 40,
  },
  menuTxtBtns: {
      fontSize: 14,
      color: '#000000',
      width: '80%',
      marginLeft: 'auto',
      marginRight: 'auto'
  },
  menuBtnsNotificacoes: {
      flexDirection: 'row',
      alignItems: 'center'
  },
  listMenu: {
      marginTop: 20
  },
  alert: {
      backgroundColor: colors.greenmoss,
      alignItems: 'center',
      justifyContent: 'center',
      width: 14,
      height: 14,
      borderRadius: 7,
      marginLeft: 10,
      marginTop: 2
  },
  alertNumber: {
      color: colors.white,
      fontSize: 9,
      textAlign: 'center',
  },
  viewSocial: {
      backgroundColor: '#000000',
      height: 50,
      width: '100%',
      position: 'absolute',
      bottom: 23,
      flexDirection: 'row',
      justifyContent: 'center'
  },
  btnSocial: {
      height: 35,
      width: 35,
      backgroundColor: '#ffffff',
      alignItems: 'center',
      justifyContent: 'center',
      marginRight: 10,
      marginTop: 7,
      borderRadius: 30
  },
  txtClose: {
      position: 'absolute',
      top: Platform.OS == 'ios'? 15 : 10,
  ...     ifIphoneX({
              top: 10,
      }),
      right: 10,
      width: 50,
      zIndex: 9,
      textAlign: 'right'
  },
  iconClose: {
      fontSize: 20
  },
  imgFace: {
      height: 23,
      width: 11,
  },
  imgTwitter: {
      height: 18,
      width: 22,
  },
  imgInstagram: {
      height: 20,
      width: 20,
  },
  imgYoutube: {
      height: 23,
      width: 20,
  }
})


export default DrawerSidebar;
