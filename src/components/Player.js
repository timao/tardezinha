import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image, Platform, Dimensions } from 'react-native';
import { fonts, colors, metrics } from '../styles';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
const { width, height } = Dimensions.get('window');

const Players = ({
}) => (
    <View>
      <LinearGradient style={styles.player} colors={['#7a63b4', '#b552a7', '#f33697']} start={{x: 0.0, y: 0.0}} end={{x: 0.7, y: 0.0}}>
        <TouchableOpacity>
          <Icon size={25} name='step-backward' color={colors.white} />
        </TouchableOpacity>
        <View style={{ width: '80%', height: 1, backgroundColor: colors.white }}></View>
        <TouchableOpacity>
          <Icon size={25} name='step-forward' color={colors.white} />
        </TouchableOpacity>
      </LinearGradient>
    </View>
);

const styles = StyleSheet.create({
  player: {
    width: '100%',
    paddingHorizontal: metrics.basePadding*2,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  }
})

export default Players;
