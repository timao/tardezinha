import { StyleSheet, Platform } from 'react-native';
import { colors, fonts, metrics } from '../../styles'

const styles = StyleSheet.create({
    container: {
        padding: metrics.basePadding*2,
        marginBottom: 140
    },
    logo: {
        width: 150,
        height: 89,
        resizeMode: 'contain',
        marginTop: 80,
        marginBottom: 50,
    },
    containerBox: {
        backgroundColor: colors.white,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        width: '100%'
    },
    h1: {
       fontFamily: fonts.r700,
       color: colors.purple,
       fontSize: 18,
       textAlign: 'center',
       width: '100%'
    },
    h2: {
       fontFamily: fonts.r700,
       color: colors.purple,
       fontSize: 18,
       textAlign: 'center',
       width: '100%',
    },
    BoxEsqueciSenha: {
        marginTop: 30
    },
    BoxButton: {
        position: 'relative',
        marginTop: 70,
    },
    butCadastrar: {
        fontFamily: fonts.r700,
        color: colors.white,
        fontSize: 16,
        width: '100%',
        marginTop: 40,
        textAlign: 'center'
    },
    buttonSocial: {
      width: '100%',
      height: 49,
      flexDirection: 'row',
      alignItems: 'center',
      marginBottom: 10,
    },
    txtSocial:{
      fontSize: 16,
      color: colors.white,
      fontFamily: fonts.r700,
      marginLeft: 15,
    },
    iconSocial: {
      marginLeft: 40,
      marginRight: 30,
    },
    wrapOu:{
      marginVertical: 30,
      alignItems: 'center',
    },
    border: {
      height: 1,
      width: '100%',
      backgroundColor: colors.borderInput,
    },
    wrapOuTxt: {
      backgroundColor: colors.white,
      width: 70,
      height: 20,
      top: -10,
      position: 'absolute',
      alignItems: 'center',
    },
    txtOu:{
      fontFamily: fonts.r400,
      fontSize: 15,
      color: colors.borderInput,
    },
    rowFoto:{
      borderRadius: 100,
      overflow: 'hidden',
      width: 110,
      height: 110,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: colors.white
    },
    photo: {
      width: '100%',
      height: '100%',
    },
    txtAceitoTermos: {
      fontFamily: fonts.r400,
      color: colors.subTxt1,
      fontSize: 18,
      paddingRight: 15,
      marginLeft: 10
    },
    sublinhado:{
      textDecorationLine: 'underline'
    },
    h1termoUso: {
      fontFamily: fonts.r700,
      color: colors.purple,
      marginTop: 10,
      fontSize: 18,
      borderBottomWidth: 1,
      borderColor: colors.purple,
      width: 120,
      marginLeft: 10

    },
    wrapAniversario: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    }
});

export default styles;
