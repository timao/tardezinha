import React, { Component } from 'react'
import { Text, View, StatusBar, TextInput, Alert, Image, BackHandler, TouchableOpacity, PermissionsAndroid, ActivityIndicator, Platform, AsyncStorage } from 'react-native'
import { withNavigationFocus } from "react-navigation";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { TextInputMask } from 'react-native-masked-text';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import LinearGradient from 'react-native-linear-gradient';
import { GoogleSignin } from 'react-native-google-signin';

import Header from '../../components/Header';
import Buttons from '../../components/Button';
import Loading from '../../components/Loading';

import { general, colors } from '../../styles'
import styles from './EditarPerfil.styles'

import config from '../../config/config';
import { facebookLogin } from '../../services/authFacebook';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as UserActions } from '../../store/ducks/user';
import { Creators as EditarPerfilActions } from '../../store/ducks/editarperfil';
import { Creators as UploadActions } from '../../store/ducks/upload';
import { Creators as FacebookActions } from '../../store/ducks/facebook';
import { Creators as GoogleActions } from '../../store/ducks/google';

const options = {
  title: 'Escolha a sua foto de perfil',
  takePhotoButtonTitle: 'Tirar Foto',
  cancelButtonTitle: 'Cancelar',
  chooseFromLibraryButtonTitle: 'Escolher da Galeria',
  quality: 0.6,
  maxWidth: 1024,
  maxHeight: 1024,
  fixOrientation: true,
  storageOptions: {
      skipBackup: true,
      path: 'images',
      waitUntilSaved: true
  },
};

class EditarPerfilPage extends Component {

  componentDidMount = async () => {

    let token = await AsyncStorage.getItem('token')
    this.props.getUserRequest(token)
    var diasemana = this.props.user.data.DataNascimento.split('T');
    var diasemana2 = diasemana[0].split('-');
    console.log(diasemana2, 'DIA SEMANA')

    this.setState({
      token: token,
      dia: diasemana2[2],
      mes: diasemana2[1],
      ano: diasemana2[0],
    })
  }
  
  handleBackButton = () => {
    this.props.navigation.goBack()
  }

  componentWillMount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);

    GoogleSignin.configure({
      scopes: ["https://www.googleapis.com/auth/drive.readonly"], // what API you want to access on behalf of the user, default is email and profile
      iosClientId: '97444700734-1rfmb9vcbuc93vkg412vkjnjcjo6637n.apps.googleusercontent.com',
      webClientId: '97444700734-bltqlrdp850cmb20nc09pdu58up634h3.apps.googleusercontent.com',
      offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      hostedDomain: '', // specifies a hosted domain restriction
      forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login
      accountName: '', // [Android] specifies an account name on the device that should be used
    })
    const { user } = this.props;
    this.setState({
      Foto: user.data.Foto,
      Nome: user.data.Nome,
      Sobrenome: user.data.Sobrenome,
      Email: user.data.Email,
      Senha: user.data.Senha,
      ConfirmarSenha: user.data.ConfirmarSenha,
      CPF: user.data.CPF,
      Celular: user.data.Celular,
      Endereco: user.data.EnderecoLogradouro,
      Cidade: user.data.EnderecoCidade,
      Cep: user.data.EnderecoCEP,
      DataNascimento: user.data.DataNascimento,
      IdFacebook: null,
      IdGoogle: null,
    })

  }

  state={
    token: null,
    Foto: '',
    Nome: '',
    Sobrenome: '',
    Email: '',
    Senha: '',
    ConfirmarSenha: '',
    CPF: '',
    Celular: '',
    Endereco: '',
    Cidade: '',
    Cep: '',
    dia: '',
    mes: '',
    ano: '',
    DataNascimento: '',
    IdFacebook: null,
    IdGoogle: null,
    AceitoConfirmacao: false,
    semconexao: null,
    loading: false,
    namephoto: '',
  }

  requestGaleriaPermission = async () => {
    try {
        await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          {
              'title': 'Cool Photo App READ_EXTERNAL_STORAGE Permission',
              'message': 'Cool Photo App needs access to your READ_EXTERNAL_STORAGE ' +
                  'so you can take awesome pictures.'
          }
        )
        await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
              'title': 'Cool Photo App CAMERA Permission',
              'message': 'Cool Photo App needs access to your CAMERA ' +
                  'so you can take awesome pictures.'
          }
        )
        await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
              'title': 'Cool Photo App WRITE_EXTERNAL_STORAGE Permission',
              'message': 'Cool Photo App needs access to your WRITE_EXTERNAL_STORAGE ' +
                  'so you can take awesome pictures.'
          }
        )
    } catch (err) {
        console.warn(err)
    }
  }

  handleCamera = async () => {
    if(Platform.OS=='android'){
      this.requestGaleriaPermission()
    }
    ImagePicker.showImagePicker(options, (response) => {
      console.log(response, 'ffoto')
      if (response.didCancel) {
        //console.log('User cancelled image picker');
      } else if (response.error) {
        //console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        //console.log('User tapped custom button: ', response.customButton);
      } else {
        if (Platform.OS == 'ios'){
          this.setState({
            Foto: response.uri,
            namephoto: response.fileName,
            loading: true,
          }
        );

        }else {
          this.setState({
            Foto: response.path,
            namephoto: response.fileName,
            loading: true,
          }
         );
        }
        this.props.getUploadRequest(this.state)
      }
    })
  }

  handleFacebook = async () => {
    // const { getLoginFBRequest } = this.props;
    const response = await facebookLogin();
    if (response.error) {
      this.setState({ error: response.error });
      //console.log(response.error);
      return false;
    }
    console.log(response, 'INFOS FACE');
    // this.props.getFacebookRequest(response);
    this.setState({
        Foto: response.user.picture.data.url,
        Nome: response.user.first_name,
        Sobrenome: response.user.last_name,
        Email: response.user.email,
        IdFacebook: response.user.id,
    })
    this.props.getFacebookRequest(this.state)

  }

  handleGoogle = async () => {
    GoogleSignin.signIn()
    .then((user) => {
      console.log(user);
      this.setState({
        Nome: user.user.givenName,
        Sobrenome: user.user.familyName,
        Email: user.user.email,
        Foto: user.user.photo,
        IdGoogle: user.user.id,
      })
      this.props.getFacebookRequest(this.state)
    })
    .catch((err) => {
      console.log('WRONG SIGNIN', err);
    })
    .done();
  };


  searchCep = (c) => {
    let dataJson = []
    let r = c.replace("-", "");
    let cepFormatado = r;
    if(c.length == 9){
    return axios.get('https://viacep.com.br/ws/' + cepFormatado + '/json')
    .then((response) => {
      console.log(response, 'RESPONSE')
      if(response.data.erro!=true){
        dataJson=response
        this.setState({
          Cep: dataJson.data.cep,
          Cidade: dataJson.data.localidade,
          Endereco: dataJson.data.bairro + ' - ' + dataJson.data.logradouro
        })
      }else if(response.data.erro==true){
        this.setState({
          Cep: '',
          Cidade: '',
          Endereco: ''
        })
      }
    })
    }else{
    console.log('não deu seis numero')
    }
  }

  validation = () => {
    if (!this.state.Nome) {
      return Alert.alert(
        config.nameApp,
        'Preencha seu Nome.'
      )
    }else if (!this.state.Sobrenome) {
      return Alert.alert(
        config.nameApp,
        'Preencha seu Sobrenome.'
      )
    }else if (!this.state.Email) {
      return Alert.alert(
        config.nameApp,
        'Preencha seu E-mail.'
      )
    }else if (!this.state.Senha) {
      return Alert.alert(
        config.nameApp,
        'Preencha sua Senha.'
      )
    }else if (!this.state.ConfirmarSenha) {
      return Alert.alert(
        config.nameApp,
        'Preencha o Confirmar Senha.'
      )
    }else if (!this.state.CPF) {
      return Alert.alert(
        config.nameApp,
        'Preencha seu CPF.'
      )
    }else if (!this.state.Celular) {
      return Alert.alert(
        config.nameApp,
        'Preencha seu Celular.'
      )
    }else if (!this.state.Endereco) {
      return Alert.alert(
        config.nameApp,
        'Preencha seu Endereco.'
      )
    }else if (!this.state.Cidade) {
      return Alert.alert(
        config.nameApp,
        'Preencha sua Cidade.'
      )
    }else if (!this.state.Celular) {
      return Alert.alert(
        config.nameApp,
        'Preencha seu Celular.'
      )
    }else if (!this.state.Cep) {
      return Alert.alert(
        config.nameApp,
        'Preencha seu Cep.'
      )
    }else if (!this.state.dia) {
      return Alert.alert(
        config.nameApp,
        'Preencha o Dia.'
      )
    }else if (!this.state.mes) {
      return Alert.alert(
        config.nameApp,
        'Preencha o Mês.'
      )
    }else if (!this.state.ano) {
      return Alert.alert(
        config.nameApp,
        'Preencha o Ano.'
      )
    }
    return true
  }

  handleEditarPerfil = () => {
    let mensagem = this.validation()
    if (mensagem == true) {
      this.props.getEditarPerfilRequest(this.state);
    }
  }



  async componentWillReceiveProps(props){
    const { editarperfil, user, upload } = props;
    console.log(editarperfil, 'EDITAR PERFIl')
    if(editarperfil.error==true){
      Alert.alert(config.nameApp, editarperfil.error_message)
      editarperfil.error=false;
    }else if(editarperfil.data.Mensagem=="Atualizado com sucesso!"){
      Alert.alert(config.nameApp, 'Perfil atualizado com sucesso!')
      editarperfil.data.Mensagem=''
      editarperfil.error=false;
      props.navigation.navigate('HomePage')
    }
    if(upload.data.Arquivo){
      this.setState({ Foto: upload.data.Arquivo, loading: false })
    }
    // if(user.data!=undefined){
    //   console.log(user.data, 'USUARIO')

    // }



  }


  render() {
    const { navigation } = this.props;
    return (
      <View style={[styles.structure, { backgroundColor: '#fff'}]}>

        <Header
          title='Editar Perfil'
          navigation={navigation}
          goback={true}
          background={['#7a63b4', '#b552a7', '#f33697']}
          photoPerfil={false}
          nocadastro={false} />
        <StatusBar backgroundColor={colors.pink} barStyle="light-content" />
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="handled"
        >
        <Loading
          loading={this.props.editarperfil.loading}
        />

          <View style={styles.container}>

            <View style={styles.wrapInformacoes}>

              <Text style={general.subText1}>Agilize a edição dos dados com</Text>
              {
                this.state.IdFacebook
                ?
                null
                : <TouchableOpacity style={[ styles.buttonSocial, { backgroundColor: colors.facebook, marginTop: 20 } ]} onPress={this.handleFacebook}>
                  <Icon
                    name='facebook-f'
                    size={22}
                    color={colors.white}
                    style={styles.iconSocial}
                  />
                  <Text style={ styles.txtSocial }>LOGIN COM FACEBOOK</Text>
                </TouchableOpacity>
              }
              {
                this.state.IdGoogle
                ?
                null
                :
              <TouchableOpacity style={[ styles.buttonSocial, { backgroundColor: colors.google } ]} onPress={this.handleGoogle}>
                <Icon
                  name='google-plus'
                  size={22}
                  color={colors.white}
                  style={styles.iconSocial}
                />
                <Text style={ styles.txtSocial }>LOGIN COM GOOGLE</Text>
              </TouchableOpacity>
              }

              <View style={styles.wrapOu}>
                <View style={styles.border}></View>
                <View style={styles.wrapOuTxt}>
                  <Text style={styles.txtOu}>OU</Text>
                </View>
              </View>

              </View>

            <View style={styles.containerBox}>
              <Text style={[general.subText2, {marginBottom: 30}]}> Informação Pessoal </Text>


              <View style={{width: 125, height: 125, position: 'relative', top: 0,
              marginLeft: 'auto',
              marginRight: 'auto' }}>
                <LinearGradient style={{width: 125, height: 125, borderRadius: 100,
                  justifyContent: 'center', alignItems: 'center',}} colors={['#f33697', '#b552a7', '#7a63b4']} start={{x: 0.0, y: 0.0}} end={{x: 0.7, y: 0.0}}>
                  <TouchableOpacity style={styles.rowFoto} onPress={() => this.handleCamera()}>
                    <ActivityIndicator style={{ position: 'absolute', left: 50, top: 45 }}/>
                  {
                    this.state.Foto
                    ?<Image
                      source={{uri: this.state.Foto}}
                      style={[styles.photo, { position: 'relative', zIndex: 1 }]}
                    />
                    :<Icon
                      name='user'
                      size={75}
                      color={colors.purple}
                      style={{ position: 'relative', zIndex: 1 }}
                    />
                  }

                  </TouchableOpacity>

                </LinearGradient>
                <TouchableOpacity style={{ width: 30, height: 30, backgroundColor: colors.pink, borderRadius: 100, justifyContent: 'center', alignItems: 'center', position: 'absolute', bottom: 10, right: 0, zIndex: 99 }}  onPress={() => this.handleCamera()}>
                <Icon
                name='plus'
                size={15}
                color={colors.white}
                />
              </TouchableOpacity>
              </View>
              <View style={general.row}>
                <Text style={general.label}> NOME* </Text>
                <TextInput
                  style={general.textInput}
                  placeholder='Digite seu nome'
                  placeholderTextColor={colors.borderInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  underlineColorAndroid="transparent"
                  value={this.state.Nome}
                  onChangeText={Nome => this.setState({ Nome })}
                  returnKeyType={'next'}
                  
                  selectTextOnFocus={false}
                  onSubmitEditing={()=>{this.sobrenome.focus();}}
                  />
              </View>

              <View style={general.row}>
                <Text style={general.label}> SOBRENOME* </Text>
                <TextInput
                  style={general.textInput}
                  ref={(input) => { this.sobrenome = input; }}
                  placeholder='Digite seu sobrenome'
                  placeholderTextColor={colors.borderInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType={'next'}
                  underlineColorAndroid="transparent"
                  selectTextOnFocus={false}
                  value={this.state.Sobrenome}
                  onChangeText={Sobrenome => this.setState({ Sobrenome })}
                  returnKeyType={'next'}
                  onSubmitEditing={()=>{this.cpf.focus();}}
                  />
              </View>

              <View style={general.row}>
                <Text style={general.label}> CPF* </Text>
                <TextInputMask
                  style={general.textInput}
                  ref={(input) => { this.cpf = input; }}
                  placeholder='Digite seu cpf'
                  placeholderTextColor={colors.borderInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  editable={false}
                  selectTextOnFocus={false}
                  returnKeyType={'next'}
                  underlineColorAndroid="transparent"
                  value={this.state.CPF}
                  onChangeText={CPF => this.setState({ CPF })}
                  returnKeyType={'next'}
                  onSubmitEditing={()=>{this.celular.focus();}}
                  type={'cpf'}
                  options={{
                      format: '999.999.999-99'
                  }}
                  />
              </View>

              <View style={general.row}>
                <Text style={general.label}> CELULAR* </Text>
                <TextInputMask
                  style={general.textInput}
                  ref={(input) => { this.celular = input; }}
                  placeholder='Digite seu celular'
                  placeholderTextColor={colors.borderInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType={'next'}
                  underlineColorAndroid="transparent"
                  value={this.state.Celular}
                  onChangeText={Celular => this.setState({ Celular })}
                  returnKeyType={'next'}
                  onSubmitEditing={()=>{this.email.focus();}}
                  type={'cel-phone'}
                  options={{
                      format: '(99) 99999-9999'
                  }}
                  />
              </View>

            </View>

            <View style={styles.containerBox}>
              <Text style={[general.subText2, {marginBottom: 10, marginTop: 40}]}> Segurança </Text>

              <View style={general.row}>
                <Text style={general.label}> E-MAIL* </Text>
                <TextInput
                  style={general.textInput}
                  ref={(input) => { this.email = input; }}
                  placeholder='Digite seu e-mail'
                  placeholderTextColor={colors.borderInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  editable={false}
                  returnKeyType={'next'}
                  underlineColorAndroid="transparent"
                  value={this.state.Email}
                  onChangeText={Email => this.setState({ Email })}
                  returnKeyType={'next'}
                  onSubmitEditing={()=>{this.senha.focus();}}
                  />
              </View>

              <View style={general.row}>
                <Text style={general.label}> SENHA* </Text>
                <TextInput
                  style={general.textInput}
                  ref={(input) => { this.senha = input; }}
                  placeholder='Digite seu senha'
                  placeholderTextColor={colors.borderInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType={'next'}
                  underlineColorAndroid="transparent"
                  secureTextEntry={true}
                  value={this.state.Senha}
                  onChangeText={Senha => this.setState({ Senha })}
                  returnKeyType={'next'}
                  onSubmitEditing={()=>{this.confirmasenha.focus();}}
                  />
              </View>
              <View style={general.row}>
                <Text style={general.label}> CONFIRMAR SENHA* </Text>
                <TextInput
                  style={general.textInput}
                  ref={(input) => { this.confirmasenha = input; }}
                  placeholder='Digite seu senha'
                  placeholderTextColor={colors.borderInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  secureTextEntry={true}
                  returnKeyType={'next'}
                  underlineColorAndroid="transparent"
                  value={this.state.ConfirmarSenha}
                  onChangeText={ConfirmarSenha => this.setState({ ConfirmarSenha })}
                  returnKeyType={'next'}
                  onSubmitEditing={()=>{this.Cepp.getElement().focus();}}
                  />
              </View>

            </View>

            <View style={styles.containerBox}>
              <Text style={[general.subText2, {marginBottom: 10, marginTop: 40}]}> Endereço </Text>

              <View style={general.row}>
                <Text style={general.label}> CEP * </Text>
                <TextInputMask
                  style={general.textInput}
                  ref={(input) => { this.Cepp = input; }}
                  placeholder='Digite seu cep'
                  placeholderTextColor={colors.borderInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType={'next'}
                  underlineColorAndroid="transparent"
                  value={this.state.Cep}
                  onSubmitEditing={()=>{this.endereco.focus();}}
                  onChangeText={this.searchCep.bind(this)}
                  type={'zip-code'}
                  options={{
                      format: '9999-999'
                  }}
                  />
              </View>

              <View style={general.row}>
                <Text style={general.label}> ENDEREÇO * </Text>
                <TextInput
                  style={general.textInput}
                  ref={(input) => { this.endereco = input; }}
                  placeholder='Digite seu endereço'
                  placeholderTextColor={colors.borderInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType={'next'}
                  underlineColorAndroid="transparent"
                  value={this.state.Endereco}
                  onChangeText={Endereco => this.setState({ Endereco })}
                  onSubmitEditing={()=>{this.cidade.focus();}}
                  />
              </View>

              <View style={general.row}>
                <Text style={general.label}> CIDADE * </Text>
                <TextInput
                  style={general.textInput}
                  ref={(input) => { this.cidade = input; }}
                  placeholder='Digite seu cidade'
                  placeholderTextColor={colors.borderInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType={'next'}
                  underlineColorAndroid="transparent"
                  value={this.state.Cidade}
                  onSubmitEditing={()=>{this.dia.focus();}}
                  onChangeText={Cidade => this.setState({ Cidade })}
                  />
              </View>



            </View>

            <View style={styles.containerBox}>
              <Text style={[general.subText2, {marginBottom: 10, marginTop: 40}]}> Aniversário </Text>
              <View style={styles.wrapAniversario}>

                <View style={[general.row, { width: '30%', borderBottomColor: colors.borderInput, borderBottomWidth: 1, paddingBottom: 0 }]}>
                  <Text style={general.label}> DIA * </Text>
                  <TextInput
                    style={[general.textInput, { borderBottomWidth: 0 }]}
                    ref={(input) => { this.dia = input; }}
                    placeholder=''
                    placeholderTextColor={colors.borderInput}
                    autoCapitalize="none"
                    autoCorrect={false}
                    returnKeyType={'next'}
                    underlineColorAndroid="transparent"
                    value={this.state.dia}
                    onSubmitEditing={()=>{this.mes.focus();}}
                    onChangeText={dia => this.setState({ dia })}
                    />
                </View>

                <View style={[general.row, { width: '30%', borderBottomColor: colors.borderInput, borderBottomWidth: 1, paddingBottom: 0 }]}>
                  <Text style={general.label}> MÊS * </Text>
                  <TextInput
                    style={[general.textInput, { borderBottomWidth: 0 }]}
                    ref={(input) => { this.mes = input; }}
                    placeholder=''
                    placeholderTextColor={colors.borderInput}
                    autoCapitalize="none"
                    autoCorrect={false}
                    returnKeyType={'next'}
                    underlineColorAndroid="transparent"
                    value={this.state.mes}
                    onSubmitEditing={()=>{this.ano.focus();}}
                    onChangeText={mes => this.setState({ mes })}
                    />
                </View>

                <View style={[general.row, { width: '30%', borderBottomColor: colors.borderInput, borderBottomWidth: 1, paddingBottom: 0 }]}>
                  <Text style={general.label}> ANO * </Text>
                  <TextInput
                    style={[general.textInput, { borderBottomWidth: 0 }]}
                    ref={(input) => { this.ano = input; }}
                    placeholder=''
                    placeholderTextColor={colors.borderInput}
                    autoCapitalize="none"
                    autoCorrect={false}
                    returnKeyType={'next'}
                    underlineColorAndroid="transparent"
                    value={this.state.ano}
                    onSubmitEditing={()=>{this.ano.focus();}}
                    onChangeText={ano => this.setState({ ano })}
                    />
                </View>

              </View>

            </View>

            <View style={styles.BoxButton}>
              <View style={styles.border}></View>
              <View style={{ marginTop: -20, marginLeft: 110 }}>
                <Buttons
                  title='ALTERAR'
                  icon='arrow-right'
                  width={200}
                  gradient={true}
                  _onPress={() => this.handleEditarPerfil()}
                  />
                </View>
            </View>

          </View>

        </KeyboardAwareScrollView>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user,
  editarperfil: state.editarperfil,
  upload: state.upload,
  facebook: state.facebook,
  google: state.google,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...UserActions,
    ...EditarPerfilActions,
    ...FacebookActions,
    ...GoogleActions,
    ...UploadActions,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)( withNavigationFocus(EditarPerfilPage));
