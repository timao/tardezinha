import { StyleSheet, Platform } from 'react-native';
import { colors, fonts, metrics } from '../../styles'

const styles = StyleSheet.create({
  wrapNumberTotal: {
    padding: metrics.basePadding,
    flexDirection: 'row',
  },
  h1: {
    fontSize: 47,
    fontFamily: fonts.r700,
    color: colors.subTxt1,
  },
  wrapAcompanhe: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  wrapList: {
    marginTop: 50,
    width: '100%',
    backgroundColor: colors.white,
    height: '100%',
    paddingBottom: 30,
    flex: 1
  },
  box: {
    width: '90%',
    backgroundColor: colors.white,
    borderRadius: 10,
    borderColor: 'rgba(238,238,238, 0.7)',
    borderWidth: 1,
    marginLeft: 'auto',
    marginRight: 'auto',
    padding: metrics.basePadding*2,
    position: 'relative',
    zIndex: 1,
    marginTop: -20
  },
});

export default styles;
