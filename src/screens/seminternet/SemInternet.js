import React from 'react';
import { StatusBar, AsyncStorage, NetInfo, Text, View, Image, ImageBackground ,BackHandler } from 'react-native';

import styles from './SemInternet.styles';
import AppLink from 'react-native-app-link';

import LinearGradient from 'react-native-linear-gradient';

import Buttons from '../../components/Button';
import Loading from '../../components/Loading';

import { general, colors, metrics } from '../../styles'

import { StackActions, NavigationActions } from "react-navigation";
import { navigatorRef } from "../../App";

class SemInternet extends React.Component {
  constructor(props) {
    super(props);
    this.state = { text: 'Useless Placeholder', net: ''  };
    NetInfo.getConnectionInfo().then((connectionInfo) => {
      this.setState({
        net: connectionInfo.type
      })
    });
  }

  verificaNet = () => {
    NetInfo.getConnectionInfo().then((connectionInfo) => {
      this.setState({
        net: connectionInfo.type
      })
    });
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton() {
      return true;
  }

  getAgain = async () => {
    let token = await AsyncStorage.getItem("token");
    let routeName = "MainRoute";
    if(!token){
      routeName = "LoginPage";
    }
    //console.log('loggg')
    this.verificaNet()
    const nav = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: routeName
        })
      ],
      key: null
    });
    if(this.state.net!='none'){
      navigatorRef.dispatch(nav);
    }else{
      //console.log('não tem net')
    }
  }

  render() {
    return (
    <ImageBackground source={require('../../assets/background/1.jpg')} style={[general.structure, { position: 'absolute', top: 0, left: 0, bottom: 0, width: '100%', height: '100%' }]}>
    <StatusBar backgroundColor={colors.pink} barStyle="light-content" />
      <View style={styles.container}>
        <Image source={require('../../assets/icons/wifi.png')} style={styles.atualizar}/>

        <Text style={styles.h1}>ERRO DE CONEXÃO</Text>

        <Text style={styles.txtDescricao}>OPS! Precisamos de internet para continuar.</Text>

        <View style={[styles.BoxButton]}>
        <Buttons
          title='TENTAR NOVAMENTE'
          width={'100%'}
          gradient={true}
          _onPress={ () => {this.getAgain()} }
          />
      </View>
    </View>
  </ImageBackground>
    );
  }
}



export default SemInternet;

