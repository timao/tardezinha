import { StyleSheet } from 'react-native';
import { colors, fonts, metrics } from '../../styles'

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1,
    width: '100%',
    height: '100%',
  },
});

export default styles;
