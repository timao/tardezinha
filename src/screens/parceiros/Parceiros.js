import React, { Component } from 'react'
import { Text, ScrollView, View, AsyncStorage, Linking } from 'react-native'

import Header from '../../components/Header'
import ListParceiros from '../../components/ListParceiros';

import { general, colors } from '../../styles'

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as ParceirosActions } from '../../store/ducks/parceiros';

class ParceirosPage extends Component {

  componentDidMount = async () => {
    let token = await AsyncStorage.getItem('token');
    this.props.getParceirosRequest(token);
  }

  handleOpenURL(url) {
    url?Linking.openURL(url):null
  }

  renderList = () => (
    <ListParceiros
      data={this.props.parceiros.data}
      open={this.handleOpenURL}
    />
  );

  render() {
    const {navigation} = this.props
    return (
      <View style={[general.structure, { backgroundColor: colors.white }]}>
        <Header
          title='Parceiros'
          background={['#FB5424', '#FA8A00', '#EFCB00']}
          photoPerfil={true}
          nocadastro={true}
          goback={true}
          navigation={navigation}
          />
          <ScrollView>
            {
              this.renderList()
            }
          </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  parceiros: state.parceiros,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...ParceirosActions,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ParceirosPage);

