import React, { Component } from 'react';
import { View, AsyncStorage, ActivityIndicator, StatusBar } from 'react-native';
import { StackActions, NavigationActions } from "react-navigation";
import { navigatorRef } from "../../App";
import { colors } from '../../styles';

class SplashPage extends Component {

  componentDidMount(){
    setTimeout(this.doRouting, 0);
  }

  doRouting = async () => {
    let token = await AsyncStorage.getItem("token");
    let first = await AsyncStorage.getItem("getFirst");
    let routeName = "MainRoute";
    if(first && !token) {
      routeName = "LoginPage";
    }else if(!first){
      routeName = "TutorialPage";
    }
    const nav = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: routeName
          })
        ],
        key: null
      });
      navigatorRef.dispatch(nav);
  }

  render() {
    return (
      <View style={{flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: colors.pink, }}>
        <StatusBar backgroundColor={colors.pink} barStyle="light-content" />

        <ActivityIndicator
        />
      </View>
    );
  }
}

export default SplashPage;
