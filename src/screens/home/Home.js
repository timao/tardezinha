import React, { Component } from 'react'
import { Text, View, FlatList, Image, StatusBar, ScrollView, AsyncStorage } from 'react-native'

import { general, colors } from '../../styles'
import styles from './Home.styles'

import HeaderHome from '../../components/HeaderHome';
import BannerHome from '../../components/BannerHome';
import Posts from '../../components/Posts';
import FlatlistInfiniteScroll from '../../components/FlatlistInfiniteScroll';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as EventosActions } from '../../store/ducks/eventos';
import { Creators as VersaoActions } from '../../store/ducks/versao';
import { Creators as TimelineActions } from '../../store/ducks/timeline';
// // To see all the requests in the chrome Dev tools in the network tab.
// XMLHttpRequest = GLOBAL.originalXMLHttpRequest ?
//     GLOBAL.originalXMLHttpRequest :
//     GLOBAL.XMLHttpRequest;

//   // fetch logger
// global._fetch = fetch;
// global.fetch = function (uri, options, ...args) {
//   return global._fetch(uri, options, ...args).then((response) => {
//     console.log('Fetch', { request: { uri, options, ...args }, response });
//     return response;
//   });
// };

const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
  const paddingToBottom = 100;
  return layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom;
};

class HomePage extends Component {

  state={
    numberOfLines: 2,
    page: 1,
    ellipsizeMode: "tail",
    quantidade: 4,
    token: null
  }

  componentDidMount = async () => {
    let token = await AsyncStorage.getItem('token')
    this.setState({
      token: token
    })
    this.props.getVersaoRequest(token)
    this.props.getEventosRequest(this.state)
    this.props.getTimelineRequest(token)
  }

  nextPage = async () => {
    await this.setState({ page: this.state.page + 1 })
  }


  renderBanner = () => (
    <BannerHome
      data={this.props.eventos.data}
      navigation={this.props.navigation}
    />
  );

  renderList () {
    const { navigation } = this.props;
    var data = this.props.timeline.data;

    return(
      <FlatlistInfiniteScroll
        itensPorPagina={5}
        page={this.state.page}
        data={this.props.timeline.data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) =>
        <Posts
          data={item}
        />
        }
      />
    )
  }

  render() {
    return (
      <View style={[general.structure, { justifyContent: 'flex-start', backgroundColor: '#fff' }]}>
        <StatusBar backgroundColor={colors.white} barStyle="dark-content" />
        <HeaderHome
        navigation={this.props.navigation}
        title='Maria Eduarda' />
        <ScrollView
          onScroll={({ nativeEvent }) => {
            if (isCloseToBottom(nativeEvent)) {
              this.nextPage();
            }
          }}
        >
          {
            this.renderBanner()
          }
          <View style={styles.wrapHome}>
            <Text style={[general.subText1, {
              width: '100%',
              textAlign: 'center',
              marginBottom: 10,
            }]}> Confira as últimas postagens </Text>
            {
              this.renderList()
            }
          </View>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  eventos: state.eventos,
  timeline: state.timeline,
  versao: state.versao,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...VersaoActions,
    ...EventosActions,
    ...TimelineActions,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
