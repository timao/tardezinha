import { StyleSheet } from 'react-native';
import { colors, fonts, metrics } from '../../styles'

const styles = StyleSheet.create({
  slide1: {
    flex: 1,
  },
  container: {
    padding: 15,
    backgroundColor: colors.backgroundBanner,
    height: 140,
    borderBottomColor: 'rgba(0, 0, 0, 0.1)',
    borderBottomWidth: 2,
  },
  img: {
      width: '100%',
      resizeMode: 'cover',
      height: 200
  },
  h1: {
      fontFamily: fonts.r700,
      fontSize: 17,
      color: colors.pink,
      marginBottom: 0,
  },
  p: {
      fontFamily: fonts.r400,
      fontSize: 16,
      color: colors.borderInput,
  },
  wrapHome: {
    marginTop: 30,
    padding: metrics.basePadding
  }
});

export default styles;
