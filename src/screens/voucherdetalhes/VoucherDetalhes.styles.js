import { StyleSheet, Platform } from 'react-native';
import { colors, fonts, metrics } from '../../styles'

const styles = StyleSheet.create({
  boxQrcode: {
    backgroundColor: colors.pink,
    width: '100%',
    zIndex: -1,
    padding: 40,
    borderRadius: 10,
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  h1: {
    fontFamily: fonts.r700,
    color: colors.pink,
    fontSize: 20,
  },
  p: {
    fontFamily: fonts.r400,
    color: colors.subTxt1,
    fontSize: 16
  },
  footer: {
    padding: 20,
    backgroundColor: colors.purple,
    borderRadius: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: 20,
  },
  containerfooter: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  font1: {
    fontFamily: fonts.r400,
    color: '#FF97C6',
    fontSize: 16,
    marginBottom: 3,
  },
  font2: {
    fontSize: 22,
    color: colors.white,
    fontFamily: fonts.r700
  }
});

export default styles;
