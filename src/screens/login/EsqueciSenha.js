import React, { Component } from 'react'
import { Alert, Text, View, ImageBackground, Image, TextInput, ScrollView, TouchableOpacity, AsyncStorage } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import IconFeather from 'react-native-vector-icons/dist/Feather';
import LinearGradient from 'react-native-linear-gradient'
import Loading from '../../components/Loading';
import { general, colors, fonts, metrics } from '../../styles'
import styles from './Login.styles'
import Buttons from '../../components/Button'
import config from '../../config/config';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as EsqueciSenhaActions } from '../../store/ducks/esquecisenha';

class EsqueciSenhaPage extends Component {

  state={
    email: '',
  }

  validation = () => {
    if (this.state.email=='') {
      return Alert.alert(
        config.nameApp,
        'Preencha seu e-mail.'
      )
    }
    return true
  }

  onClick = () => {
    let mensagem = this.validation()
    if (mensagem == true) {
      this.props.getEsqueciSenhaRequest(this.state)
    }
  }

  async componentWillReceiveProps(props){
    const { esquecisenha, navigation } = props;
    console.log(esquecisenha, 'ESQUECI SENHA')
    if(esquecisenha.sucess==true){
      esquecisenha.loading=false
      setTimeout(() => {
        Alert.alert(config.nameApp, esquecisenha.data,
          [
            {text: 'OK', onPress: () => navigation.goBack()},
          ])
        esquecisenha.sucess=false
      }, 100);
    }else if(esquecisenha.error==true){
      esquecisenha.loading=false
      setTimeout(() => {
        Alert.alert(config.nameApp, esquecisenha.error_message)
        esquecisenha.error=false;
      }, 100);

    }
  }

  render() {
    const { navigation } = this.props;
    return (
      <ImageBackground source={require('../../assets/background/1.jpg')} style={{ width: '100%', height: '100%', resizeMode: 'contain' }}>
        <ScrollView>
        <Loading
            loading={this.props.esquecisenha.loading}
          />
          <TouchableOpacity onPress={ () => navigation.goBack()} style={{ position: 'absolute', top: 25, right: 15 }}>
            <Image source={require('../../assets/icons/closed.png')} style={{height: 20, resizeMode: 'contain'}}/>
          </TouchableOpacity>
          <View style={{ marginTop: 100, flex: 1, justifyContent: 'center', alignItems: 'center', paddingHorizontal: metrics.basePadding*2, height: '100%', }}>

            <LinearGradient style={{ width: 80, height: 80, borderRadius: 100, justifyContent: 'center', alignItems: 'center', position: 'relative', top: 20, zIndex: 999}} colors={['#7a63b4', '#b552a7', '#f33697']} start={{x: 0.0, y: 0.0}} end={{x: 0.7, y: 0.0}}>
              <View style={{ width: 75, height: 75, borderRadius: 100, justifyContent: 'center', alignItems: 'center',backgroundColor: colors.white }}>
                <Image source={require('../../assets/icons/checkbox.png')} style={{height: 30 , resizeMode: 'contain'}}/>
              </View>
            </LinearGradient>
            <View style={[styles.containerBox, { paddingTop: 40, paddingBottom: 50, height: 300, justifyContent: 'center', alignItems: 'center' }]}>
              <Text style={{ fontFamily: fonts.r400, color: colors.subTxt2, fontSize: 20, width: '100%', textAlign: 'center', marginTop: 15, }}>Esqueci minha senha</Text>
              <Text  style={{ fontFamily: fonts.r400, color: colors.subTxt1, fontSize: 16, width: '85%', textAlign: 'center', justifyContent: 'center', alignItems: 'center', marginTop: 15, marginBottom: 20, marginLeft: 'auto', marginRight: 'auto', }}>Digite abaixo seu e-mail cadastrado, para que você possa alterar sua senha. Te enviaremos um e-mail para redefinição da senha.</Text>

              <View style={[general.row, {paddingHorizontal: metrics.basePadding*2}]}>
                <TextInput
                  style={[general.textInput, { fontSize: 14 }]}
                  placeholder='E-MAIL'
                  placeholderTextColor={colors.pink}
                  onSubmitEditing={() => this.onClick()}
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType={'next'}
                  underlineColorAndroid="transparent"
                  value={this.state.email}
                  keyboardType="email-address"
                  textContentType="emailAddress"
                  onChangeText={email => this.setState({ email })}
                  />
              </View>

            </View>
            <View style={[styles.BoxButton, { top: -30 }]}>
              <View style={[{ backgroundColor: 'rgba(0, 0, 0, 0.4)', width: '100%', height: 50, borderRadius: 30, }]}>
                <Buttons
                  title='ENVIAR E-MAIL'
                  width={'100%'}
                  gradient={true}
                  _onPress={ () => this.onClick() }
                  />
              </View>
            </View>
          </View>
        </ScrollView>
      </ImageBackground>
    )
  }
}

const mapStateToProps = state => ({
  esquecisenha: state.esquecisenha,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...EsqueciSenhaActions,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(EsqueciSenhaPage);

