import React, { Component } from 'react'
import { Alert, Text, View, ImageBackground, NetInfo,  Image, TextInput, ScrollView, TouchableOpacity, AsyncStorage, StatusBar, BackHandler, Platform } from 'react-native'
import { withNavigationFocus } from "react-navigation";
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import { GoogleSignin } from 'react-native-google-signin';
import * as RNIap from 'react-native-iap';

import Buttons from '../../components/Button';
import Loading from '../../components/Loading';

import { general, colors, metrics } from '../../styles'
import styles from './Login.styles'
import config from '../../config/config';
import { facebookLogin } from '../../services/authFacebook';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as FacebookActions } from '../../store/ducks/facebook';
import { Creators as LoginActions } from '../../store/ducks/login';
import { Creators as AssinaturaActions } from '../../store/ducks/assinatura';


const itemSkus = Platform.select({
  ios: [
    'assinatura.tardizinha_001',
  ],
  android: [
    'assinatura.tardezinha_001',
  ],
});

class LoginPage extends Component {
  constructor(props){
    super(props)

    this.state={
      facebook: false,
      google: false,
      // email: 'am4mobile@gmail.com',
      // password: '123mudar',
      email: '',
      password: '',
      semconexao: false,
      emailfb: '',
      passwordfb: '',
      GoogleSubscriptionId: '',
      GooglePlayId: '',
      GooglePurchaseToken: '',
      AppStoreSubscriptionId: '',
      AppStoreId: '',
      AppStorePurchaseToken: '',
    }
    NetInfo.getConnectionInfo().then((connectionInfo) => {
      if(connectionInfo.type=='none'){
        this.setState({
          semconexao: true
        })
      }
    });
  }



  handleBackButton() {
    return true;
  }

  componentDidMount = async () => {
    try {
      const result = await RNIap.initConnection();
      await RNIap.consumeAllItems();
      console.log('result', result);
    } catch (err) {
      console.warn(err.code, err.message);
    }

    try {
      // const products = await RNIap.getProducts(itemSkus);
      const products = await RNIap.getSubscriptions(itemSkus);
      console.log('Products', products);
      this.setState({ productList: products });
    } catch (err) {
      console.warn(err.code, err.message);
    }
  }

  componentWillMount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);

    GoogleSignin.configure({
      scopes: ["https://www.googleapis.com/auth/drive.readonly"], // what API you want to access on behalf of the user, default is email and profile
      iosClientId: '97444700734-1rfmb9vcbuc93vkg412vkjnjcjo6637n.apps.googleusercontent.com',
      webClientId: '97444700734-bltqlrdp850cmb20nc09pdu58up634h3.apps.googleusercontent.com',
      offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      hostedDomain: '', // specifies a hosted domain restriction
      forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login
      accountName: '', // [Android] specifies an account name on the device that should be used
    })
  }

  loginFB = async () => {
    const response = await facebookLogin();
    if (response.error) {
      this.setState({ error: response.error });
      return false;
    }
    this.setState({
      user: response,
      facebook: true,
      emailfb: 'facebook:' + response.user.id,
      passwordfb: response.accessToken,
    })
    console.log(this.state, 'INFOS FACE');
    setTimeout(() => {
      this.props.getLoginRequest(this.state);
    }, 100);
  }


  handleGoogle = async () => {
    GoogleSignin.signIn()
    .then((user) => {
      this.setState({
      user: user,
      google: true,
      emailfb: 'google:' + user.user.id,
      passwordfb: user.accessToken,
      })

      console.log(user, 'USERRR')
      console.log(this.state, 'estado')
      setTimeout(() => {
        this.props.getLoginRequest(this.state);
      }, 900);
    })
    .catch((err) => {
      console.log('WRONG SIGNIN', err);
    })
    .done();
  };


  validation = () => {
    if (this.state.email=='') {
      return Alert.alert(
        config.nameApp,
        'Você precisa informar seu e-mail e senha.'
      )
    }else if (this.state.password=='') {
      return Alert.alert(
        config.nameApp,
        'Você precisa informar seu e-mail e senha.'
      )
    }
    return true
  }

  login = async () => {
    let mensagem = this.validation()
    if (mensagem == true) {
      if(this.state.semconexao==true){
          this.props.navigation.navigate('SemInternet')
        }else{
          this.props.getLoginRequest(this.state);
      }
    }
  }

  async loginSucess(token){
    const { navigation } = this.props;
    await AsyncStorage.setItem('token', token)
    navigation.navigate('MainRoute')
  }

  componentWillUnmount(){
    this.props.login.error=false;
  }

  async componentWillReceiveProps(props){
    const { login, assinatura } = props;
    if(login.error_message==='Usuário ou senha inválidos'){
      login.loading=false
      Platform.OS==='ios'
      ?setTimeout(() => {
        Alert.alert(config.nameApp, 'O e-mail e/ou senha informados estão errados.', [
          {text: 'OK',
            onPress: () => {
              login.error_message='';
            }
          }
        ])
          login.error=false
          if(this.state.facebook==true){
          login.error=false
          this.props.navigation.navigate('CadastroPage', {dadosFb: this.state})
        }else if(this.state.google==true){
          login.error=false
          this.props.navigation.navigate('CadastroPage', {dadosgoogle: this.state})
        }
        login.error=false;
      }, 100)
      :await Alert.alert(config.nameApp, 'O e-mail e/ou senha informados estão errados.', [
        {text: 'OK',
          onPress: () => {
            login.error_message='';
          }
        }
      ])
        login.error=false
        if(this.state.facebook==true){
        login.error=false
        this.props.navigation.navigate('CadastroPage', {dadosFb: this.state})
      }else if(this.state.google==true){
        login.error=false
        this.props.navigation.navigate('CadastroPage', {dadosgoogle: this.state})
      }
      login.error=false;
    }
    // }else if(login.error_message==='Assinatura Cancelada'){
    //   login.loading=false
    //   Platform.OS==='ios'
    //   ?setTimeout(() => {
    //     Alert.alert(config.nameApp, 'Assinatura Cancelada', [
    //       {text: 'OK',
    //         onPress: () => {
    //           login.error_message='';
    //           this.buyItem();
    //         }
    //       }
    //     ])
    //     login.error=false;
    //   }, 100)
    //   : Alert.alert(config.nameApp, 'Assinatura Cancelada', [
    //     {text: 'OK',
    //       onPress: () => {
    //         login.error_message='';
    //         this.buyItem();
    //       }
    //     }
    //   ])


    // }else if(login.error_message==='Assinatura não encontrada'){
    //   login.loading=false
    //   Platform.OS==='ios'
    //   ?setTimeout(() => {
    //     Alert.alert(config.nameApp, 'Assinatura não encontrada', [
    //       {text: 'OK',
    //         onPress: () => {
    //           login.error_message='';
    //           this.buyItem();
    //         }
    //       }
    //     ])
    //     login.error=false;
    //   }, 100)
    //   :await Alert.alert(config.nameApp, 'Assinatura não encontrada', [
    //     {text: 'OK',
    //       onPress: () => {
    //         login.error_message='';
    //         this.buyItem();
    //       }
    //     }
    //   ])
    //   login.error=false;

    // }else if(login.error_message==='Assinatura Expirada'){
    //   login.loading=false
    //   Platform.OS==='ios'
    //   ?setTimeout(() => {
    //     Alert.alert(config.nameApp, 'Assinatura Expirada', [
    //       {text: 'OK',
    //         onPress: () => {
    //           login.error_message='';
    //           this.buyItem();
    //         }
    //       }
    //     ])
    //     login.error=false;
    //   }, 100)
    //   :await Alert.alert(config.nameApp, 'Assinatura Expirada', [
    //     {text: 'OK',
    //       onPress: () => {
    //         login.error_message='';
    //         this.buyItem();
    //       }
    //     }
    //   ])
    //   login.error=false;
    // }
  }

  buyItem = async () => {
    console.log('clicando')
    if(Platform.OS==='android'){
      if(this.state.GooglePlayId==''){
      try {
        const purchase  = await RNIap.buySubscription(this.state.productList[0].productId);
        console.log(purchase, 'purchasseee')
        this.setState({
          GoogleSubscriptionId: purchase.productId,
          GooglePlayId: purchase.transactionId,
          GooglePurchaseToken: purchase.purchaseToken,
        })
        this.handleAssinatura()
      } catch (err) {
        console.log(err.code, err.message, 'ERRO');
      }
      }else{
        this.handleAssinatura()
      }
    }else if(Platform.OS==='ios'){
      if(this.state.AppStorePurchaseToken==''){
        try {
          const purchase  = await RNIap.buyProduct(this.state.productList[0].productId);
          console.log(purchase, 'purchasseee')
          this.setState({
            AppStoreSubscriptionId: purchase.productId,
            AppStoreId: purchase.transactionId,
            AppStorePurchaseToken: purchase.transactionReceipt,
          })
          this.handleAssinatura()
        } catch (err) {
          console.log(err.code, err.message, 'ERRO');
        }
      }else{
        this.handleAssinatura()
      }
    }
  }

  handleAssinatura = () => {
    console.log(this.state, 'STATEEE')
    this.props.getAssinaturaRequest(this.state)
  }

  async componentWillUnmount(){
    const { login } = this.props;
    login.error=false;
    login.data=[]
  }


  render() {
    const { navigation, isFocused } = this.props;

    if (isFocused)
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    else
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);

    return (
      <ImageBackground source={require('../../assets/background/1.jpg')} style={[general.structure, { position: 'absolute', top: 0, left: 0, bottom: 0, width: '100%', height: '100%' }]}>
        <StatusBar backgroundColor={colors.pink} barStyle="light-content" />
        <ScrollView>

        <Loading
          loading={this.props.login.loading}
        />
          <View style={general.structure}>
            <View style={styles.container}>
              <Image source={require('../../assets/logo.png')} style={styles.logo}/>

              <View style={styles.containerBox}>
                <Text style={styles.h1}>LOGIN</Text>

                <View style={styles.boxSocial}>
                  <Text style={styles.txtSocial}>ENTRAR COM</Text>
                  <TouchableOpacity onPress={() => this.loginFB()}>
                  <LinearGradient style={styles.buttonSocial} colors={['#7a63b4', '#b552a7', '#f33697']} start={{x: 0.0, y: 0.0}} end={{x: 0.0, y: 0.7}}>
                    <Icon name='facebook-f' size={20} color={colors.white} />
                  </LinearGradient>
                  </TouchableOpacity>
                  <TouchableOpacity  onPress={() => this.handleGoogle()}>
                  <LinearGradient style={styles.buttonSocial} colors={['#7a63b4', '#b552a7', '#f33697']} start={{x: 0.0, y: 0.0}} end={{x: 0.0, y: 0.7}}>
                    <Icon name='google-plus' size={20} color={colors.white} />
                  </LinearGradient>
                  </TouchableOpacity>
                </View>

                <TouchableOpacity style={[styles.BoxEsqueciSenha, {margin: 0}]}>
                  <Text style={styles.h2}>Ou</Text>
                </TouchableOpacity>


                <View style={[general.row, {paddingHorizontal: metrics.basePadding*2}]}>
                  <TextInput
                    style={general.textInput}
                    placeholder='E-MAIL'
                    placeholderTextColor={colors.pink}
                    onSubmitEditing={()=>{this.ipt_password.focus();}}
                    autoCapitalize="none"
                    autoCorrect={false}
                    returnKeyType={'next'}
                    underlineColorAndroid="transparent"
                    value={this.state.email}
                    keyboardType="email-address"
                    textContentType="emailAddress"
                    onChangeText={email => this.setState({ email })}
                    />
                </View>

                <View style={[general.row, {paddingHorizontal: metrics.basePadding*2}]}>
                  <TextInput
                    ref={(input) => { this.ipt_password = input; }}
                    style={general.textInput}
                    returnKeyType="go"
                    secureTextEntry={true}
                    onSubmitEditing={() => this.login()}
                    textContentType="password"
                    placeholder='SENHA'
                    placeholderTextColor={colors.pink}
                    autoCapitalize="none"
                    autoCorrect={false}
                    underlineColorAndroid="transparent"
                    value={this.state.password}
                    onChangeText={password => this.setState({ password })}
                    />
                </View>

                <TouchableOpacity style={styles.BoxEsqueciSenha} onPress={() => navigation.navigate('EsqueciSenhaPage')}>
                  <Text style={styles.h2}>Esqueci minha senha</Text>
                </TouchableOpacity>

                <View style={styles.BoxButton}>
                  <Buttons
                    title='ENTRAR'
                    width={'75%'}
                    gradient={true}
                    _onPress={ () => this.login() }
                    />
                </View>
              </View>
            </View>

            <View style={{ marginBottom: 30 }}>
              <TouchableOpacity onPress={() => navigation.navigate('CadastroPage')}>
                <Text style={styles.butCadastrar}>CADASTRAR</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </ImageBackground>
    )
  }
}

const mapStateToProps = state => ({
  assinatura: state.assinatura,
  login: state.login,
  facebook: state.facebook,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...LoginActions,
    ...AssinaturaActions,
    ...FacebookActions,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)( withNavigationFocus(LoginPage));
