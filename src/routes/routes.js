import React from 'react';
import {
  createStackNavigator,
    createBottomTabNavigator,
    DrawerNavigator
} from 'react-navigation';
import { Image } from 'react-native'
import TabBottomBar from '../components/TabBottomBar';

import { colors, metrics } from '../styles/index'

//Stack
import HomeStackNavigator from './homeStack'
import AgendaStackNavigator from './agendaStack'
import MaisStackNavigator from './maisStack'
import EventoPage from '../screens/evento/Evento'
import ParceirosPage from '../screens/parceiros/Parceiros'
import AjudaPage from '../screens/ajuda/Ajuda'
import ContatoPage from '../screens/contato/Contato'
import VoucherDetalhesPage from '../screens/voucherdetalhes/VoucherDetalhes'

//Sreens
import SplashPage from '../screens/splash/Splash'
import TutorialPage from '../screens/tutorial/Tutorial'
import LoginPage from '../screens/login/Login'
import EsqueciSenhaPage from '../screens/login/EsqueciSenha'
import CadastroPage from '../screens/cadastro/Cadastro'
import CadastroSucessoPage from '../screens/cadastro/Cadastro_Success';
import FazeroBemPage from '../screens/fazerobem/FazeroBem'
import LojinhaPage from '../screens/lojinha/Lojinha'
import EditarPerfilPage from '../screens/editarperfil/EditarPerfil'
import SemInternet from '../screens/seminternet/SemInternet';
import VersaoPage from '../screens/versao/Versao';
import TermoUso from '../screens/termouso/TermoUso';

// Configurações da TabBar
const MainRoute = createBottomTabNavigator({
  HomePage: {
    screen: HomeStackNavigator,
    navigationOptions:{
      tabBarLabel: 'TARDEZINHA',
      tabBarIcon: ({ focused,tintColor }) => (
      focused ?
        <Image source={require('../assets/tab/1_active.png')} style={{ width: 30, height: 27, resizeMode: 'contain' }} />
        :
        <Image source={require('../assets/tab/1.png')} style={{ width: 30, height: 27, resizeMode: 'contain' }} />
      ),
    },
  },
  AgendaStackNavigator: {
    screen: AgendaStackNavigator,
    navigationOptions:{
      tabBarLabel: 'AGENDA',
      tabBarIcon: ({ focused, tintColor }) => (
      focused ?
        <Image source={require('../assets/tab/2_active.png')} style={{ width: 30, height: 27, resizeMode: 'contain' }} />
        :
        <Image source={require('../assets/tab/2.png')} style={{ width: 30, height: 27, resizeMode: 'contain' }} />
      ),
    },
  },
  FazeroBemPage: {
    screen: FazeroBemPage,
    navigationOptions:{
      tabBarLabel: 'FAZER O BEM',
      tabBarIcon: ({ focused,tintColor }) => (
      focused ?
        <Image source={require('../assets/tab/3_active.png')} style={{ width: 30, height: 27, resizeMode: 'contain' }} />
        :
        <Image source={require('../assets/tab/3.png')} style={{ width: 30, height: 27, resizeMode: 'contain' }} />
      ),
    },
  },
  LojinhaPage: {
    screen: LojinhaPage,
    navigationOptions:{
      tabBarLabel: 'LOJINHA',
      tabBarIcon: ({ focused,tintColor }) => (
      focused ?
        <Image source={require('../assets/tab/4_active.png')} style={{ width: 30, height: 27, resizeMode: 'contain' }} />
        :
        <Image source={require('../assets/tab/4.png')} style={{ width: 30, height: 27, resizeMode: 'contain' }} />
      ),
    },
  },
  MaisStackNavigator: {
    screen: MaisStackNavigator,
    navigationOptions:{
      tabBarLabel: 'MAIS',
      tabBarIcon: ({ focused,tintColor }) => (
      focused ?
        <Image source={require('../assets/tab/5_active.png')} style={{ width: 30, height: 27, resizeMode: 'contain' }} />
        :
        <Image source={require('../assets/tab/5.png')} style={{ width: 30, height: 27, resizeMode: 'contain' }} />
      ),
    },
  },
},
  {
  tabBarComponent: TabBottomBar,
  tabBarPosition: 'bottom',
  tabBarOptions: {
    showIcon: true,
    showLabel: true,
    activeTintColor: colors.gray,
    inactiveTintColor: '#B9B9B9',
    labelStyle: {
      fontSize: 8,
    },
    style: {
      backgroundColor: colors.white,
      borderTopColor: '#000',
      fontSize: 13,
      shadowOpacity: 0.75,
      shadowRadius: 5,
      shadowColor: 'red',
      shadowOffset: { height: 0, width: 0 },
    },
  },
});

// const MainRoute = createStackNavigator({
//   TabBarMain: { screen: TabBarMain },
//   EditarPerfilPage: { screen: EditarPerfilPage }
// },
// {
//   headerMode: 'none'
//  });

const Logged = createStackNavigator({
  MainRoute: {
    screen: MainRoute
  }
},{ headerMode: 'none' })

const NoLogged = createStackNavigator({
  LoginPage: {
      screen: LoginPage
  },
  EsqueciSenhaPage: {
      screen: EsqueciSenhaPage
  },
  CadastroPage: {
    screen: CadastroPage,
  },
  CadastroSucessoPage: {
    screen: CadastroSucessoPage,
  },
},
    {
        headerMode: "none",
    }
);

const MainNavigation = createStackNavigator({
  SplashPage: { screen: SplashPage },
  TutorialPage: { screen: TutorialPage },
  LoginPage: { screen: LoginPage },
  NoLogged: { screen: NoLogged },
  SemInternet: { screen: SemInternet },
  VersaoPage: { screen: VersaoPage },
  TermoUso: { screen: TermoUso },
  MainRoute: { screen: MainRoute },
  EditarPerfilPage: { screen: EditarPerfilPage },
  EventoPage: { screen: EventoPage },
  ParceirosPage: { screen: ParceirosPage },
  AjudaPage: { screen: AjudaPage },
  ContatoPage: { screen: ContatoPage },
  VoucherDetalhesPage: { screen: VoucherDetalhesPage },
},{
    headerMode: "none",
})

const AppNavigator = (MainNavigation);

export default AppNavigator;
